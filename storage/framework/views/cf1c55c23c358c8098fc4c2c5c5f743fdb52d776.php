<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <h2><?php echo app('translator')->getFromJson('profile.profile_page_public_link'); ?></h2>
        <a href="#" target="_blank" id="publicLinkEn" ><?php echo app('translator')->getFromJson('profile.profile_page_public_link_en'); ?></a>
        <br>
        <a href="#" target="_blank" id="publicLinkMk" ><?php echo app('translator')->getFromJson('profile.profile_page_public_link_mk'); ?></a>
    </div>
</div>