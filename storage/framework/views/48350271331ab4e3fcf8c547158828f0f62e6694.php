<div class="infoContainer" style="margin-top: 2em">

    <h2 class="text-uppercase p-1"><?php echo e($user->first_name." ".$user->last_name); ?></h2>
    <i  class="fa fa-pencil fa-2x pointer " id="editUserInfo" aria-hidden="true"></i>

        <h4><?php echo e($user->job); ?></h4>
        <div class="d-flex-center justify-flex-start p-1"><i class="fa fa-map-marker fa-2x" aria-hidden="true"></i><span><?php if($user->city_id !==   null): ?><?php echo e($user->city->name); ?><?php endif; ?></span></div>
        <div class="d-flex-center justify-flex-start p-1"><i class="fa fa-graduation-cap fa-2x" aria-hidden="true"></i><span><?php echo e($user->degree); ?></span></div>
        <div class="d-flex-center justify-flex-start p-1"><i class="fa fa-suitcase fa-2x" aria-hidden="true"></i><span><?php echo e($user->job); ?></span></div>
        <div class="d-flex-center justify-flex-start p-1"><i class="fa fa-envelope fa-2x" aria-hidden="true"></i><span><?php echo e($user->email); ?></span></div>
        <div class="d-flex-center justify-flex-start p-1"><i class="fa fa-phone fa-2x" aria-hidden="true"></i><span><?php echo e($user->phone); ?></span></div>
        
            


</div>
<h2 class="text-center"><?php echo app('translator')->getFromJson('profile.profile_page_work_experience'); ?></h2>
<?php $__currentLoopData = $currentWorkExperience; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $experience): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="experienceContainer">
        <div class="d-flex-center experienceContainerIconContainer">
            
            <div class="experienceContainerIcon d-flex-center" style="height: 5em; width: 5em; background-color: <?php echo e($user->color); ?>; border-radius: 50%; border: 5px solid white"><i class="fa fa-suitcase fa-2x text-white" style="margin-right: 0"></i></div>
        </div>
        <h4 class="text-center text-muted"><?php echo e($experience->from); ?> - present</h4>
        <h4 class="text-center m-top-bottom-2 experienceType"><?php echo e($experience->position); ?>  <?php echo app('translator')->getFromJson('profile.profile_page_at'); ?> <a href="<?php echo e($experience->webpage); ?>" style="color: #333333; font-weight: 700;text-decoration: underline "><?php echo e($experience->company); ?></a> </h4>
        <p class="text-center m-top-bottom-2"><?php echo e($experience->info); ?></p>
    </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__currentLoopData = $experiences; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $experience): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


<div class="experienceContainer">
    <div class="d-flex-center experienceContainerIconContainer">
        
        <div class="experienceContainerIcon d-flex-center" style="height: 5em; width: 5em; background-color: <?php echo e($user->color); ?>; border-radius: 50%; border: 5px solid white"><i class="fa fa-suitcase fa-2x text-white" style="margin-right: 0"></i></div>
    </div>
    <h4 class="text-center text-muted"><?php echo e($experience->from); ?> - <?php echo e($experience->to); ?></h4>
    <h4 class="text-center m-top-bottom-2 experienceType"><?php echo e($experience->position); ?>  <?php echo app('translator')->getFromJson('profile.profile_page_at'); ?> <a href="<?php echo e($experience->webpage); ?>" style="color: #333333; font-weight: 700;text-decoration: underline"><?php echo e($experience->company); ?></a>  </h4>
    <p class="text-center m-top-bottom-2"><?php echo e($experience->info); ?></p>
</div>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<h2 class="text-center"><?php echo app('translator')->getFromJson('profile.profile_page_education'); ?></h2>

<?php $__currentLoopData = $educationExperience; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $experience): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

    <div class="experienceContainer">
        <div class="d-flex-center experienceContainerIconContainer">
            
            <div class="experienceContainerIcon d-flex-center" style="height: 5em; width: 5em; background-color: <?php echo e($user->color); ?>; border-radius: 50%; border: 5px solid white"><i class="fa fa-graduation-cap fa-2x text-white" style="margin-right: 0"></i></div>
        </div>
        <h4 class="text-center text-muted"><?php echo e($experience->from); ?> - <?php echo e($experience->to); ?></h4>
        <h4 class="text-center m-top-bottom-2 experienceType"><?php echo e($experience->position); ?>  <?php echo app('translator')->getFromJson('profile.profile_page_at'); ?> <a href="<?php echo e($experience->webpage); ?>" style="color: #333333; font-weight: 700; text-decoration: underline"><?php echo e($experience->company); ?></a>  </h4>
        <p class="text-center m-top-bottom-2"><?php echo e($experience->info); ?></p>
    </div>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<h2 class="text-center"><?php echo app('translator')->getFromJson('profile.profile_page_projects'); ?></h2>

<?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $experience): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

    <div class="experienceContainer">
        <div class="d-flex-center experienceContainerIconContainer">
            
            <div class="experienceContainerIcon d-flex-center" style="height: 5em; width: 5em; background-color: <?php echo e($user->color); ?>; border-radius: 50%; border: 5px solid white"><i class="fa fa-laptop fa-2x text-white" style="margin-right: 0"></i></div>
        </div>
        <h4 class="text-center text-muted"><?php echo e($experience->from); ?> - <?php echo e($experience->to); ?></h4>
        <h4 class="text-center m-top-bottom-2 experienceType"><a href="<?php echo e($experience->webpage); ?>" style="color: #333333; font-weight: 700; text-decoration: underline"><?php echo e($experience->position); ?></a></h4>

        <p class="text-center m-top-bottom-2"><?php echo e($experience->info); ?></p>
    </div>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    
    




<?php if(($public === false) && $user->id === Auth::id()): ?>
    <?php echo $__env->make('profile.modals.userInfoModal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('profile.modals.colorModal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('profile.modals.experienceModal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('profile.modals.shareProfileModal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script src="<?php echo e(asset('js/profilePageRightSection.js')); ?>"></script>

<?php endif; ?>