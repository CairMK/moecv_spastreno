<div class="modal fade" id="colorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                
            </div>
            <div class="modal-body">
                <div class="colorsContainer d-flex-center" style="flex-wrap: wrap">
                    <?php $__currentLoopData = $colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="colorCircle d-flex-center" data-color="<?php echo e($color); ?>" style="height: 5em; width: 5em; border-radius: 50%; background-color: <?php echo e($color); ?> ; margin: 1em"><?php if($user->color === $color): ?><i style="color: white" class="fa fa-check fa-lg"></i><?php endif; ?></div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal"><?php echo app('translator')->getFromJson('profile.profile_page_close'); ?></button>
                
            </div>
        </div>
    </div>
</div>