 <div class="container-fluid">

	<div class="row">  

		<div class="col-md-12 p-0">
			<div class="profile-header d-flex-center" style="background-color: <?php echo e($user->color); ?>">
                <i class="fa fa-quote-left fa-5x quote-left" aria-hidden="true"></i>
                <div class="profile-header-text text-center" id="profileHeaderText">
                    <?php if($user->profile_header_text !== null): ?>
                        <?php echo e($user->profile_header_text); ?>

                    <?php else: ?>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus eligendi expedita harum iure laborum qui! Eius iste iure quas .
                    <?php endif; ?>
                </div>
                <i class="fa fa-quote-right fa-5x quote-right" aria-hidden="true"></i>
                <?php if($public === false): ?>
                
                <?php endif; ?>
            </div>
		</div>	

	</div>

</div>

 <?php echo $__env->make('profile.modals.profileHeaderModal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

 <script src="<?php echo e(asset('js/profileHeader.js')); ?>"></script>