    <?php $__env->startSection('head'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('css/profile/leftSection.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('css/profile/rightSection.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('css/profile/header.css')); ?>" />
    <meta name="theme-color" content="<?php echo e($user->color); ?>">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="no-cache">
    <meta http-equiv="Expires" content="-1">
    <meta http-equiv="Cache-Control" content="no-cache">
    <?php if(($public === false) && $user->id === Auth::id()): ?>
        <link rel="stylesheet" href="<?php echo e(asset('css/profile/leftSectionAuth.css')); ?>" />
        
        
    <?php endif; ?>

    <link rel="stylesheet" href="<?php echo e(asset('EasyAutocomplete-1.3.5/easy-autocomplete.css')); ?>" />
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
    <script src="<?php echo e(asset('EasyAutocomplete-1.3.5/jquery.easy-autocomplete.js')); ?>"></script>


    <script src="<?php echo e(asset('js/jquery.tinycolorpicker.js')); ?>"></script>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('navbar'); ?>
    <?php if($public === false): ?>
    <?php echo $__env->make('navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php else: ?>
        <div class="d-flex-center">
            <h4>Креирано со</h4>
            <a style="margin-left: 1em; font-weight: 700" href="http://moecv.mk"> Moe CV</a>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div id="pdfContent">
    


    <?php echo $__env->make('profile.profile-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-1">
                <?php echo $__env->make('profile.left-section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>

            <div class="col-md-6">


                <?php echo $__env->make('profile.rightSection', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <?php if($public === false): ?>
    <?php echo $__env->make('profile.downloadpdf', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
    <script>
        var height = $( document ).height();
        var width = $( document ).width();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.post('/saveScreenDimensions',{height:height,width:width},function (data) {
            console.log(data);
        });


    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('defaultLayout.defaultLayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>