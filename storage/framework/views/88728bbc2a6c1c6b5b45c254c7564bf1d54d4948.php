<nav class="navbar navbar-inverse" style="background-color: <?php echo e($user->color); ?>; border-radius: 0; border: none; margin-bottom: 0">


    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand text-white" href="#" style="color: white;font-weight: 700"><?php echo app('translator')->getFromJson('mainPage.main_page_app_name'); ?></a>
        </div>


        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo e(route('slider')); ?>" style="color: white;font-weight: 700"><?php echo app('translator')->getFromJson('profile.profile_page_edit_button'); ?></a></li>
                                <li><a href="<?php echo e(route('logout')); ?>" style="color: white;font-weight: 700"onclick="event.preventDefault();document.getElementById('ogout-form').submit();">
                    <?php echo app('translator')->getFromJson('profile.profile_page_logout_button'); ?>
                </a>
                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                    <?php echo e(csrf_field()); ?>

                </form>
            </li>
        </ul>
    </div>
</div>
</nav>