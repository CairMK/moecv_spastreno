<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = ['Велес','Демир Капија','Кавадарци','Неготино','Свети Николе','Берово','Виница','Делчево','Кочани','Македонска Каменица','Пехчево','Пробиштип','Штип','Дебар','Кичево ','Македонски Брод','Богданци','Валандово','Гевгелија','Радовиш','Струмица','Битола','Демир Хисар','Крушево','Прилеп','Ресен','Гостивар','Тетово','Кратово','Крива Паланка','Куманово','Скопје',];

        foreach ($cities as $city){
            $cityModel = new City();
            $cityModel->name = $city;
            $cityModel->save();
        }
    }
}
