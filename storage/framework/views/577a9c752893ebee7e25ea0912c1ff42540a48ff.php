<div class="profile-card-container d-flex-center flex-direction-column">
    <div class="profile-image-container"
         <?php if($user->profile_photo !== null): ?>
         style="background-image: url('<?php echo e(asset('storage/'.$user->profile_photo)); ?>')"
            <?php endif; ?>
    ></div>
    
    <h4><?php echo app('translator')->getFromJson('profile.available_text'); ?></h4>

    <div class="profile-icons-container d-flex-center pointer">
        <?php if($user->available_full_time !== null): ?>
            <div class="d-flex-center flex-direction-column m-1 icon-container">
                <?php if($user->available_full_time): ?>
                    <a href="#"><i class="fa fa-check fa-2x" style="color: <?php echo e($user->color); ?>; margin-right: 0"></i></a>
                <?php else: ?>
                    <a href="#"><i class="fa fa-close fa-2x" style="color: <?php echo e($user->color); ?>; margin-right: 0"></i></a>
                <?php endif; ?>
                <p class="text-center"><?php echo app('translator')->getFromJson('profile.profile_page_full_time'); ?></p>

            </div>

            <div class="d-flex-center flex-direction-column m-1 icon-container">
                <?php if($user->available_part_time): ?>
                    <a href="#"><i class="fa fa-check fa-2x" style="color: <?php echo e($user->color); ?>; margin-right: 0"></i></a>
                <?php else: ?>
                    <a href="#"><i class="fa fa-close fa-2x" style="color: <?php echo e($user->color); ?>; margin-right: 0"></i></a>
                <?php endif; ?>
                <p class="text-center"><?php echo app('translator')->getFromJson('profile.profile_page_part_time'); ?></p>
            </div>

            <div class="d-flex-center flex-direction-column m-1 icon-container">
                <?php if($user->available_intern): ?>
                    <a href="#"><i class="fa fa-check fa-2x" style="color: <?php echo e($user->color); ?>; margin-right: 0"></i></a>
                <?php else: ?>
                    <a href="#"><i class="fa fa-close fa-2x" style="color: <?php echo e($user->color); ?>; margin-right: 0"></i></a>
                <?php endif; ?>
                <p class="text-center"><?php echo app('translator')->getFromJson('profile.profile_page_freelance'); ?></p>
            </div>
            
                
                     
                    
                
            
        <?php else: ?>
            
                
                    
                
            
        <?php endif; ?>


    </div>
    

    <div class="profile-skills-container">
        <p class="text-center" style="font-weight: 700;margin-top: 2em"><?php echo app('translator')->getFromJson('profile.profile_page_top_three_skills'); ?></p>
        <div class="top-three-skills">

            <?php $__currentLoopData = $user->topThreeSkills->reverse(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $skill): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <p style="background-color: <?php echo e($user->color); ?>; color: white"><?php echo e($skill->name); ?></p>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php if(($public === false) && $user->id === Auth::id()): ?>
                
            <?php endif; ?>
        </div>
        <p class="text-center" style="font-weight: 700;margin-top: 2em"><?php echo app('translator')->getFromJson('profile.profile_page_additional_skills'); ?></p>
        <div class="additional-skills">
        <?php $__currentLoopData = $user->skills->reverse(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $skill): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <p style="background-color: <?php echo e($user->color); ?>; color: white"><?php echo e($skill->name); ?></p>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php if(($public === false) && $user->id === Auth::id()): ?>
                
                    
            <?php endif; ?>

        </div>
    </div>
    
    <div class="experienceContainer" style="width: 100%">
        <div class="d-flex-center experienceContainerIconContainer">
            
            <div class="experienceContainerIcon d-flex-center" style="height: 5em; width: 5em; background-color: <?php echo e($user->color); ?>; border-radius: 50%; border: 5px solid white"><i class="fa fa-globe fa-2x text-white" style="margin-right: 0"></i></div>
        </div>
        <h4 class="text-center text-muted"><?php echo app('translator')->getFromJson('profile.profile_page_language'); ?></h4>
       <?php $__currentLoopData = $user->languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <h4><?php echo e($language->name); ?></h4>
        <p class="text-muted" style="padding-bottom: 2em"><?php echo e($language->level); ?></p>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    
    <div class="social-icons-container d-flex-start flex-direction-column pointer"
         <?php if($user->id === Auth::id()): ?>id="socialMedia"<?php endif; ?>>
        <?php if(count($user->socialNetworks) > 0): ?>

            <?php $__currentLoopData = $user->socialNetworks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $socialNetwork): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if(trim($socialNetwork->pivot->url) !== '' ): ?>
                    <a href="<?php echo e($socialNetwork->pivot->url); ?>"><i class="fa fa-<?php echo e($socialNetwork->name); ?>"
                                                                style="background-color: <?php echo e($user->color); ?>"
                                                                aria-hidden="true"></i><span
                                style="margin-left: 2em; color: <?php echo e($user->color); ?>"><?php echo e($socialNetwork->pivot->url); ?></span></a>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </div>
    
    
        
            

        
        
        
            
        

    
</div>






<!-- Modal -->
<?php if(($public === false) && $user->id === Auth::id()): ?>
    <?php echo $__env->make('profile.modals.uploadPhotoModal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('profile.modals.availabilityModal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('profile.modals.topThreeSkillsModal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('profile.modals.additionalSkillsModal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('profile.modals.bestAdviceModal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
    <script src="<?php echo e(asset('js/profilePage.js')); ?>"></script>
<?php endif; ?>